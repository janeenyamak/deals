# this is for the google credentials: export GOOGLE_APPLICATION_CREDENTIALS="bigquerykey.json"
import json
from agileCrm import agileCRM
from google.cloud import bigquery
from upsertContactsDeals import CheckContactsDeals
from upsertDeals import checkDealExists

client = bigquery.Client()
responses = agileCRM("opportunity?page_size=50", "GET", None, "application/json")

print("Upserting Deals Table ")
isDone = False
for i, response in enumerate(responses):
    archived = response["archived"]
    print("\nCurrently at Index", i, "DealID:", response["id"])

    exsists = checkDealExists(response["id"], archived)

    if exsists == None:
        print("Deal Has Been Archived \n")
    elif exsists:
        query_job = client.query(
            """
          UPDATE `novvum-analytics.AgileCRM.Deals`
          SET milestone = '{}'
          WHERE id = {}
        """.format(
                response["milestone"], response["id"]
            )
        )
        print("Query has been updated! \n")
    else:
        query_job = client.query(
            """
          INSERT INTO  `novvum-analytics.AgileCRM.Deals` (id, name, milestone, close_date,created_time,owner_email,owner_name )
          VALUES ({},'{}','{}',{},{},'{}','{}')
        """.format(
                response["id"],
                response["name"],
                response["milestone"],
                "null",
                response["created_time"],
                response["owner"]["email"],
                response["owner"]["name"],
            )
        )
        print("New row has been added! \n")
###########
isDone = True
## Upsert ContactsDeals ##
if isDone:
    print("\nNow Upserting ContactsDeals...")
    for i, response in enumerate(responses):
        print("\n")
        print("Index ", i)

        archived = response["archived"]
        print("contact_id(s)", response["contact_ids"])

        for conID in response["contact_ids"]:
            newRows = []
            rowExists = CheckContactsDeals(response["id"], conID, archived, newRows)
            print("Does Row Exist: ", rowExists)

            if rowExists == None:
                print("deal_id Has Been Archived")
                break
            elif rowExists == False:
                if len(response["contact_ids"]) == 0:
                    print("There Are No ContactId(s) Listed Within This DealId")
                    continue
                print("Table Needs Updating Inserting", len(newRows), "New Rows\n")
                for i in newRows:
                    for key, value in i.items():
                        print(
                            "Inserting deal_id:",
                            key,
                            "and contact_id into table ",
                            value,
                        )
                        query_job = client.query(
                            """
                    INSERT INTO `novvum-analytics.AgileCRM.ContactsDeals` (deal_id, contact_id)
                    VALUES ({},{})

                  """.format(
                                key, value
                            )
                        )
                        print("deal_id(s) and contact_id(s) Have Been Added\n")
            else:
                print("Nothing to Update\n")
                continue

print("\nAll Done!")

