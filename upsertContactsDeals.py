from agileCrm import agileCRM
from google.cloud import bigquery

client = bigquery.Client()


def CheckContactsDeals(dealId, contactId, isarchived, newRows):

    query_job = client.query(
        """
            SELECT distinct deal_id, contact_id
            FROM `novvum-analytics.AgileCRM.ContactsDeals`
            WHERE deal_id = {} and contact_id = {}
          """.format(
            dealId, contactId
        )
    )
    # print("Query job is: ", query_job)
    rows = query_job.result()  # Waits for query to finish
    if len(contactId) == 0:
        return False
    if rows.total_rows == 0:  # then deal_Id does not exists
        addNewRow = {}  # Row that is going to added to the list
        addNewRow[dealId] = contactId
        newRows.append(addNewRow)
        print("List of items to be added is: ", newRows)
        return False

    for i, row in enumerate(rows):
        print(
            "\nCheck:",
            str(dealId),
            str(contactId),
            "==",
            str(row.get("deal_id")),
            str(row.get("contact_id")),
        )
        if isarchived:
            return None
        elif str(dealId) + str(contactId) == str(row.get("deal_id")) + str(
            row.get("contact_id")
        ):
            # print("dealId = ", dealId, " contactID = ", contactId)
            print("Combination Exist")
            continue
        else:
            print("Combination Does Not Exist")
            addNewRow = {}
            addNewRow[dealId] = contactId
            newRows.append(addNewRow)

    if len(newRows) == 0:
        return True
    else:
        return False

