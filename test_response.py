test_response = [
    {
        "id": 123,
        "name": "Testing Will Delete",
        "contact_ids": ["12345676543212345"],
        "milestone": "test",
        "created_time": 98765678,
        "archived": False,
        "owner": {
            "id": 987654345678,
            "domain": "test",
            "email": "test@novvum.io",
            "name": "delete",
        },
    },
    {
        "id": 456,
        "name": "Hello World",
        "contact_ids": ["23456543234"],
        "milestone": "test",
        "created_time": 34564,
        "archived": False,
        "owner": {
            "id": 87654567,
            "domain": "test",
            "email": "hello@novvum.io",
            "name": "hello E",
        },
    },
]

