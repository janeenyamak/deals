from agileCrm import agileCRM
from google.cloud import bigquery

client = bigquery.Client()
responses = agileCRM("opportunity?page_size=50", "GET", None, "application/json")


def checkDealExists(dealId, isArchived):
    found = False
    query_job = client.query(
        """
          SELECT id
          FROM `novvum-analytics.AgileCRM.Deals`
          WHERE id = {}
        """.format(
            dealId
        )
    )
    rows = query_job.result()  # Waits for query to finish

    if rows.total_rows == 0:  # Row DNE within the Table
        return False  # Return False in order to add Row to BigQuery

    for row in rows:
        print("row_id is ", row.id, " deal_id is ", dealId)
        if isArchived:
            # print("This Deal Has Been Archived")
            found = None
            break
        else:
            if row.id == dealId:
                found = True
                break  # Stop when Found
            else:
                found = False
    return found

